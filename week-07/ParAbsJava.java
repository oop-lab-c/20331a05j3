import java.util.*;

abstract class base {
   abstract void show();
    void wish() {
        System.out.println("hello");
    }
}

class ParAbsJava extends base {
    void show() {
        System.out.println("This is Java");
    }

    public static void main(String[] args) {

        ParAbsJava s = new ParAbsJava();
        s.wish();

        s.show();
       

}
}

